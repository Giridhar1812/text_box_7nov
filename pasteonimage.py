# -*- coding: utf-8 -*-
"""
Created on Sat Oct 14 23:16:11 2017

@author: divi
"""

import Image
import sys
sys.path.append('/home/divi/text_box')
from detect import detect_boxes


logo_size = 200,200

img_background = Image.open('/home/divi/text_box/samples/test_images/3.jpg')

#img_background_width , img_background_height = img_background.size

img_logo = Image.open('/home/divi/text_box/samples/test_images/logo1.jpg')

img_logo.thumbnail(logo_size,Image.ANTIALIAS)

#img_logo_width , img_logo_height = img_logo.size

Converted_logo_image=img_logo.convert("RGBA")

Converted_background_image=img_background.convert("RGBA")

for empty_space in detect_boxes("test_images/ake1150150200079.jpg"):


#mask_image=Image.new("1",img_logo.size,"white")

    Converted_background_image_copy=Converted_background_image.copy()

    new_image = Converted_background_image_copy.crop(empty_space)

#Image.blend(Converted_logo_image,new_image,0.5).save('/home/divi/text_box/samples/test_images/blended_output.jpg')

    Converted_background_image.paste(Image.blend(Converted_logo_image,new_image,0.3), (empty_space),mask=Image.blend(Converted_logo_image,new_image,0.3))

#Converted_background_image.paste(Image.blend(Converted_logo_image,new_image,0.3), (0,500))

#Converted_background_image.paste(Image.blend(Converted_logo_image,new_image,0.3), (0,800))

    Converted_background_image.save('/home/divi/text_box/samples/test_images/output1.jpg')

