# -*- coding: utf-8 -*-
"""
Created on Sat Oct 14 23:16:11 2017

@author: divi
"""

import Image

import ImageFilter

logo_size = 200,200

img_background = Image.open('/home/divi/text_box/samples/test_images/3.jpg')

img_background_width , img_background_height = img_background.size

img_logo = Image.open('/home/divi/text_box/samples/test_images/logo1.jpg')

img_logo_png = Image.open('/home/divi/text_box/samples/test_images/logo1.png')

img_logo.thumbnail(logo_size,Image.ANTIALIAS)

img_logo_width , img_logo_height = img_logo.size

Converted_logo_image=img_logo.convert("RGBA")

Converted_background_image=img_background.convert("RGBA")

mask_image=Image.new("1",img_logo.size,"white")

Converted_background_image_copy=Converted_background_image.copy()

new_image = Converted_background_image_copy.crop((0,0,200,200))

new_filter = new_image.filter(ImageFilter.MinFilter).save('/home/divi/text_box/samples/test_images/filter_output.jpg')

Image.blend(Converted_logo_image,new_image,0.5).save('/home/divi/text_box/samples/test_images/blended_output.jpg')

Converted_background_image.paste(Image.blend(Converted_logo_image,new_image,0.3), (0,0),mask=Image.blend(Converted_logo_image,new_image,0.3))

Converted_background_image.paste(Image.blend(Converted_logo_image,new_image,0.3), (0,500))

Converted_background_image.paste(Image.blend(Converted_logo_image,new_image,0.3), (0,800))

Converted_background_image.save('/home/divi/text_box/samples/test_images/output1.jpg')

