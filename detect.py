import numpy as np
import os
from skimage import io
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
from PIL import Image
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
from pathlib import Path
import detect
#BASE path needs to be edited to make it work

CODE_BASE_PATH =os.path.dirname(detect.__file__)+'/'
sys.path.append(CODE_BASE_PATH)

#utility modules added

from utils import label_map_util
from utils import visualization_utils as vis_util


def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)

def get_filepaths(directory):
    """
    This function will generate the file names in a directory 
    tree by walking the tree either top-down or bottom-up. For each 
    directory in the tree rooted at directory top (including top itself), 
    it yields a 3-tuple (dirpath, dirnames, filenames).
    """
    file_paths = []  # List which will store all of the full filepaths.

    # Walk the tree.
    for root, directories, files in os.walk(directory):
        for filename in files:
            # Join the two strings in order to form the full filepath.
            if ("jpg" in filename) or ("jpeg" in filename) or ("png" in filename) :
                filepath = os.path.join(root, filename)
                file_paths.append(filepath)  # Add it to the list.

    return file_paths  # Self-explanatory.

# Run the above function and store its results in a variable.   

def detect_boxes(image_path,pred_path='pred_images',object_flag=False,visualize_flag=False):
	# What model to download.
	MODEL_NAME = 'ssd_mobilenet_v1_coco_11_06_2017'
	MODEL_FILE = MODEL_NAME + '.tar.gz'

	# Path to frozen detection graph. This is the actual model that is used for the object detection.
	PATH_TO_CKPT = CODE_BASE_PATH+MODEL_NAME + '/frozen_inference_graph.pb'

	# List of the strings that is used to add correct label for each box.
	PATH_TO_LABELS = CODE_BASE_PATH+os.path.join('data', 'mscoco_label_map.pbtxt')

	NUM_CLASSES = 90
	os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

	response_list=[]
	detection_graph = tf.Graph()
	with detection_graph.as_default():
	  od_graph_def = tf.GraphDef()
	  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
	    serialized_graph = fid.read()
	    od_graph_def.ParseFromString(serialized_graph)
	    tf.import_graph_def(od_graph_def, name='')


	label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
	categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
	category_index = label_map_util.create_category_index(categories)
	PATH_TO_TEST_IMAGES_DIR = image_path
	p = Path(image_path)
	if p.is_dir() == True :
		TEST_IMAGE_PATHS = get_filepaths(PATH_TO_TEST_IMAGES_DIR)
	elif p.is_file() == True and (("jpg" in image_path) or ("jpeg" in image_path) or ("png" in image_path)):
		TEST_IMAGE_PATHS=[image_path]
	else:
		print 'invalid path',image_path
		return response_list
	#print TEST_IMAGE_PATHS
	# Size, in inches, of the output images.
	IMAGE_SIZE = (12, 8)

	with detection_graph.as_default():
	  with tf.Session(graph=detection_graph) as sess:
	    for image_path in TEST_IMAGE_PATHS:
	      image = Image.open(image_path)
	      file_base_url, filename = os.path.split(image_path)
	      # the array based representation of the image will be used later in order to prepare the
	      # result image with boxes and labels on it.
	      image_np = load_image_into_numpy_array(image)
	      # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
	      image_np_expanded = np.expand_dims(image_np, axis=0)
	      image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
	      # Each box represents a part of the image where a partic# For the sake of simplicity we will use only 2 images:
	      boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
	      # Each score represent how level of confidence for each of the objects.
	      # Score is shown on the result image, together with the class label.
	      scores = detection_graph.get_tensor_by_name('detection_scores:0')
	      classes = detection_graph.get_tensor_by_name('detection_classes:0')
	      num_detections = detection_graph.get_tensor_by_name('num_detections:0')
	      # Actual detection.
	      (boxes, scores, classes, num_detections) = sess.run(
		  [boxes, scores, classes, num_detections],
		  feed_dict={image_tensor: image_np_expanded})
	      # Visualization of the results of a detection.
	      algorithm_coordinates,objects_with_scores = vis_util.visualize_boxes_and_labels_on_image_array(
		  image_np,
		  np.squeeze(boxes),
		  np.squeeze(classes).astype(np.int32),
		  np.squeeze(scores),
		  category_index,
		  use_normalized_coordinates=True,
		  line_thickness=8,
		  visualize=visualize_flag)
	      print image_path,' processed with result as ',algorithm_coordinates
	      if object_flag:
		      response_list.append((filename,algorithm_coordinates,objects_with_scores))
	      else:
		      #response_list.append((filename,algorithm_coordinates))
		      response_list.append(algorithm_coordinates)
	      if visualize_flag:
		      plt.figure(figsize=IMAGE_SIZE)
		      plt.imshow(image_np)
		      im = Image.fromarray(image_np)
		      if not os.path.exists(pred_path):
			pred_path_full=os.path.dirname(os.path.realpath(__file__))+pred_path
			os.makedirs(pred_path)
		      im.save(pred_path+"/"+str(filename))
	return response_list


if __name__ == "__main__":
	detect_boxes('test_images',pred_path='pred_images',object_flag=False,visualize_flag=False)

#sample call for a single image
#detect_boxes('text_box/test_images/ake1150150200079.jpg',visualize_flag=True)

#sample call for a directory
#detect_boxes('test_images',visualize_flag=True)


