# -*- coding: utf-8 -*-
"""
Created on Sat Oct 14 23:16:11 2017

@author: divi
"""

import Image
import sys
sys.path.append('/home/divi/text_box')
from detect import detect_boxes
from PIL import ImageFont
from PIL import ImageDraw 
import dircache
import random
import textwrap

colors  = ["#FF5733","#4CAF50","#FF1744","#AB47BC","#2196F3","#26A69A","#76FF03"]
dir='/home/divi/text_box/fonts'
logo_size = 200,200

img_background = Image.open('/home/divi/text_box/samples/test_images/camera.jpg')

#img_background_width , img_background_height = img_background.size

img_logo = Image.open('/home/divi/text_box/samples/test_images/logo2.png')

img_logo.thumbnail(logo_size,Image.ANTIALIAS)

img_logo_width , img_logo_height = img_logo.size

Converted_logo_image=img_logo.convert("RGBA")

Converted_background_image=img_background.convert("RGBA")

detectbox=detect_boxes("test_images/camera.jpeg")

print detect_boxes("test_images/camera.jpg")


for empty_space in detect_boxes("test_images/camera.jpg"):

    Coordinates=tuple (empty_space)
    x=Coordinates[0]+100
    y=Coordinates[1]+100
    x1=x+img_logo_width
    y1=y+img_logo_width
    
    print Coordinates
    
    box=(x,y,x1,y1)
    print ("1. Co-ordinates for logo :"),box
    
    x2=Coordinates[2]-100
    y2=Coordinates[3]-100
    x3=x2-img_logo_width
    y3=y2-img_logo_width
    
    box1=(x3,y3,x2,y2)
    print ("2. Co-ordinates for text :"),box1
    
    x5=Coordinates[0]+100
    y5=Coordinates[3]-100
    x4=x+img_logo_width
    y4=y2-img_logo_width
    
    box2=(x5,y5,x4,y4)
    print ("2. Co-ordinates for logo :"),box2
    
    x7=Coordinates[3]-100
    y7=Coordinates[1]+100
    x6=x+img_logo_width
    y6=y2-img_logo_width
    
    box3=(x6,y6,x7,y7)
    print ("2. Co-ordinates for text :"),box3
    
    #mask_image=Image.new("1",img_logo.size,"white")

    Converted_background_image_copy=Converted_background_image.copy()

    new_image = Converted_background_image_copy.crop(box)

    #Image.blend(Converted_logo_image,new_image,0.5).save('/home/divi/text_box/samples/test_images/blended_output.jpg')

    #Converted_background_image.paste(Converted_logo_image,(x,y,x1,y1),Converted_logo_image)

    #Converted_background_image.paste(Image.blend(Converted_logo_image,new_image,0.3), (0,500))

    #Converted_background_image.paste(Image.blend(Converted_logo_image,new_image,0.3), (0,800))
    Converted_background_image1=Converted_background_image 

    Converted_background_image.save('/home/divi/text_box/samples/test_images/output_new.jpg')
    draw = ImageDraw.Draw(Converted_background_image)
    #txt = Image.new('RGBA', Converted_background_image_copy.size, (255,255,255,0))
    # txt_layer=ImageDraw.Draw(txt)

    random_font=filename = random.choice(dircache.listdir(dir));
  
    font = ImageFont.truetype("//home/divi/text_box/fonts/"+random_font, 32)
    quote="Think Diffrent ! Stay Hungry !! Stay Foolish !!"
   
    rect = Image.new('RGBA', (1024,512))
    Rectdraw = ImageDraw.Draw(rect)
    Rectdraw.rectangle((box),fill=(0,0,0,45),outline=None)
    Rectdraw.rectangle((box1),fill=(0,0,0,45),outline=None)
    Rectdraw.rectangle((box2),fill=(0,0,0,45),outline=None)
    Rectdraw.rectangle((box3),fill=(0,0,0,45),outline=None)
    
    #draw.text((x+250,y,x1+500,y1+300),quote,random.choice(colors),font=font)
    txt_clr=random.choice(colors)
    margin = offset = 40
    for line in textwrap.wrap(quote, width=40):
      #draw.text((margin, offset), line, font=font, fill="#aa0000")
      Rectdraw.text(((x+offset,y1+offset)),line,txt_clr,font=font)
      offset += font.getsize(line)[1]    
    
    Converted_background_image.paste(rect,mask=rect)
    out = Image.alpha_composite(Converted_background_image, Converted_background_image1)
    out.paste(Converted_logo_image,(x,y,x1,y1),Converted_logo_image)
    out.save('/home/divi/text_box/samples/test_images/out.jpg')
    Converted_background_image.save('/home/divi/text_box/samples/test_images/output_font_only_sample1.jpg')

