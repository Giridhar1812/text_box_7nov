# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 08:53:26 2017

@author: divi
"""
import Image
from PIL import ImageFont
from PIL import ImageDraw 
import random
import dircache

colors  = ["#FF5733","#4CAF50","#FF1744","#AB47BC","#2196F3","#26A69A","#76FF03"]
dir='/home/divi/text_box/fonts'


Converted_background_image =Image.open('/home/divi/text_box/samples/test_images/steve.jpg')



draw = ImageDraw.Draw(Converted_background_image)

start_x=300
start_y=300

random_font=filename = random.choice(dircache.listdir(dir));
font = ImageFont.truetype("//home/divi/text_box/fonts/"+random_font, 32)
draw.text((start_x,start_y),"Think Diffrent ! Stay Hungry , Stay Foolish !!",random.choice(colors),font=font)
Converted_background_image.save('/home/divi/text_box/samples/test_images/output_font_only.jpg')