
#the sample code to show how to pass image directory and get the results
#"test_images" is the input directory

import sys

# add the path directory where the source code is extracted in the system
sys.path.append('/home/divi/text_box')
from detect import detect_boxes
for image_path,coordinates in detect_boxes("test_images",):
	print image_path
	print coordinates


