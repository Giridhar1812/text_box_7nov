# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 13:03:26 2017

@author: divi
"""

from PIL import Image, ImageDraw, ImageFont
import random,dircache


random_font=filename = random.choice(dircache.listdir(dir));
  
font = ImageFont.truetype("//home/divi/text_box/fonts/"+random_font, 32)
# get an image
base = Image.open("test_images/ake1150150200079.jpg").convert('RGBA')

# make a blank image for the text, initialized to transparent text color
txt = Image.new('RGBA', base.size, (255,255,255,0))

# get a font
fnt = ImageFont.truetype('Pillow/Tests/fonts/FreeMono.ttf', 40)
# get a drawing context

d = ImageDraw.Draw(txt)
d.rectangle(((0,0),(100,100)),fill="#4CAF50",outline=None)
# draw text, half opacity
d.text((10,10), "Hello", font=fnt, fill=(255,255,255,128))
# draw text, full opacity
d.text((10,60), "World", font=fnt, fill=(255,255,255,255))

out = Image.alpha_composite(base, txt)

out.show()