

Installation steps
Run the below commands in terminal
# For CPU
pip install tensorflow
# For GPU
pip install tensorflow-gpu
sudo apt-get install protobuf-compiler python-pil python-lxml
sudo pip install jupyter
sudo pip install matplotlib
sudo pip install pillow
sudo pip install lxml
sudo pip install jupyter
sudo pip install matplotlib
sudo pip install pathlib
sudo apt-get install -y python-skimage
sudo apt-get install python-tk

Add Libraries to PYTHONPATH
note down the pull path of downloaded directory
###example export PYTHONPATH=/home/jithen/Documents/raydar/08sep/text_box
export PYTHONPATH=$PYTHONPATH:`pwd`:<<<noted_dir>>>>


Usage instructions:
See the sample code for execution

Sample Run:
jithen@dev:~/Documents/samples$ python sample1.py
test_images/ake1150150200079.jpg  processed with result as  (5, 5, 245, 612)
ake1150150200079.jpg
(5, 5, 245, 612)


jithen@dev:~/Documents/samples$ python sample2.py
test_images/ake1150150200079.jpg  processed with result as  (5, 5, 245, 612)
test_images/ake1150170600122.jpg  processed with result as  (1005, 5, 1295, 611)
ake1150150200079.jpg
(5, 5, 245, 612)
ake1150170600122.jpg
(1005, 5, 1295, 611)


jithen@dev:~/Documents/samples$ python sample3.py
test_images/ake1150150200079.jpg  processed with result as  (5, 5, 245, 612)
test_images/ake1150170600122.jpg  processed with result as  (1005, 5, 1295, 611)
ake1150150200079.jpg
(5, 5, 245, 612)
['dining table: 71%']
['person: 99%']
['person: 98%']
ake1150170600122.jpg
(1005, 5, 1295, 611)
['person: 88%']
['person: 90%']
['person: 93%']
['book: 86%']

jithen@dev:~/Documents/samples$ python sample4.py
test_images/ake1150150200079.jpg  processed with result as  (5, 5, 245, 612)
test_images/ake1150170600122.jpg  processed with result as  (1005, 5, 1295, 611)
ake1150150200079.jpg
(5, 5, 245, 612)
['dining table: 71%']
['person: 99%']
['person: 98%']
ake1150170600122.jpg
(1005, 5, 1295, 611)
['person: 88%']
['person: 90%']
['person: 93%']
['book: 86%']


