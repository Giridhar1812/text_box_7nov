# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is for processing images and get the bounding box for filling text to highlight the advertisement.
* Version 1.0


### How do I get set up? ###

* Summary of set up
	Run the below commands in terminal
	# For CPU
	pip install tensorflow
	# For GPU
	pip install tensorflow-gpu
	sudo apt-get install protobuf-compiler python-pil python-lxml
	sudo pip install jupyter
	sudo pip install matplotlib
	sudo pip install pillow
	sudo pip install lxml
	sudo pip install jupyter
	sudo pip install matplotlib
	sudo pip install pathlib
	sudo apt-get install -y python-skimage
	sudo apt-get install python-tk

* How to run tests
	Add Libraries to PYTHONPATH
	note down the pull path of downloaded directory
	###example export PYTHONPATH=/home/jithen/Documents/raydar/08sep/text_box
	export PYTHONPATH=$PYTHONPATH:`pwd`:<<<noted_dir>>>>
* Deployment instructions
	check sample folder for demo

